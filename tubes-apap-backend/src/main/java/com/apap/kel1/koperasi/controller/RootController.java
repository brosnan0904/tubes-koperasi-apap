package com.apap.kel1.koperasi.controller;

import com.apap.kel1.koperasi.model.RoleModel;
import com.apap.kel1.koperasi.service.RoleService;
import com.apap.kel1.koperasi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Controller
public class RootController {

    @Autowired
    RoleService roleService;

    @RequestMapping("/")
    private String home(Model model) {

        List<RoleModel> listRole = roleService.getAllRole();
        model.addAttribute("listRole",listRole);
        return "home";
    }

    @RequestMapping("/login")
    public String login(){
        return "login";
    }
}