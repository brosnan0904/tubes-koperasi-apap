package com.apap.kel1.koperasi.controller;

import com.apap.kel1.koperasi.model.AnggotaModel;
import com.apap.kel1.koperasi.service.AnggotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AnggotaController {

    @Autowired
    private AnggotaService anggotaService;

    @GetMapping(value = "/anggota")
    public List<AnggotaModel> getAllAnggota() {
        return anggotaService.getAllAnggota();
    }
}