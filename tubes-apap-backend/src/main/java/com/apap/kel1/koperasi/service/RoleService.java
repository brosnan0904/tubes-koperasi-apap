package com.apap.kel1.koperasi.service;


import com.apap.kel1.koperasi.model.RoleModel;

import java.util.List;

public interface RoleService {

    RoleModel getRoleById (int id);

    List<RoleModel> getAllRole();
}