package com.apap.kel1.koperasi.controller;

import com.apap.kel1.koperasi.model.RoleModel;
import com.apap.kel1.koperasi.model.UserModel;
import com.apap.kel1.koperasi.service.RoleService;
import com.apap.kel1.koperasi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    private String addUserSubmit(@ModelAttribute UserModel user, @RequestParam("role_id") int role_id,
                                 BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            return "error";
        }

        //user.setRole(roleService.getRoleById(6));
        user.setRole(roleService.getRoleById(role_id));
        userService.addUser(user);

        return "home";
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    private List<UserModel> getAll(){
        return userService.getAllUser();
    }

}