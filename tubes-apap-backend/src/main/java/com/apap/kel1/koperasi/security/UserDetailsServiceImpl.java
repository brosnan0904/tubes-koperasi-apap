package com.apap.kel1.koperasi.security;


import com.apap.kel1.koperasi.model.RoleModel;
import com.apap.kel1.koperasi.model.UserModel;
import com.apap.kel1.koperasi.repository.RoleDb;
import com.apap.kel1.koperasi.repository.UserDb;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;


@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService{
    @Autowired
    private UserDb userDb;

    @Autowired
    private RoleDb roleDb;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserModel user = userDb.findByUsername(username);
        RoleModel role = roleDb.findById(user.getRole().getId());
        System.out.println(role.getNama());
        Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
        grantedAuthorities.add(new SimpleGrantedAuthority(role.getNama()));

        return new User(user.getUsername(), user.getPassword(), grantedAuthorities);
    }
}